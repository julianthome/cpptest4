#include <iostream>
#include <cstring>

int test() {
    char x[20];
    char y[20];
    int i = 10;

    // test
    memcpy(x, y, i); // detected
    memcpy(y, "2100{", 2); // detected
    //memcpy(x, "2100{", 2);
    //memcpy(y, "2100{", 2);
    memcpy(x, "2100{", 2); // not detected
    memcpy(y, "2100{", 2); // not detected
    memcpy(y, "2100{", 2); // detected
    if (true) {
        memcpy(y, "2100{", 2); // detected
    }
    memcpy(y, "2100{", 2); // not detected
    memcpy(y, "2100{", 2); // detected
    return 0;
}